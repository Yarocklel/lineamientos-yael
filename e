[33mcommit cd1ba7eca1876361452c81dabacbf0c4c4025989[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;32mTopic[m[33m, [m[1;32mDevelop[m[33m)[m
Author: Yael Soto <xyarock@gmail.com>
Date:   Thu Jan 5 01:10:44 2023 -0600

    commit desde la rama topic

[33mcommit b0c7eb4219a3ad88ed82b37a3ebdadccf9f20119[m
Merge: 33c15d2 d1978a2
Author: Yael Soto <xyarock@gmail.com>
Date:   Thu Jan 5 01:04:36 2023 -0600

    Merge branch 'BUG-LINEA-001'

[33mcommit 33c15d2a45be21f896c76ebb30bfd731ff9a653d[m
Author: Yael Soto <xyarock@gmail.com>
Date:   Thu Jan 5 01:01:54 2023 -0600

    commit en la rama hotfix

[33mcommit d1978a227c9f1567fd40407d2fadc5e364c28cb2[m
Author: Yael Soto <xyarock@gmail.com>
Date:   Thu Jan 5 00:59:30 2023 -0600

    Commit en la rama bug

[33mcommit 789481ea3c35cf115a32ea4908657d6898e9b797[m
Author: Yael Soto <xyarock@gmail.com>
Date:   Thu Jan 5 00:57:42 2023 -0600

    Commit en la rama master

[33mcommit de203a9d43170a4bd85be8bbeb69a28f7f051b2f[m[33m ([m[1;31morigin/master[m[33m, [m[1;31morigin/TR-LINEA-Antonio-Martin[m[33m, [m[1;31morigin/TR-LINEA-AlanPaz[m[33m, [m[1;31morigin/TR-LINEA-ANDRE[m[33m, [m[1;31morigin/Rama-EdgarRuiz[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Jorge Enrique Cervantes Silva <jorge.cervantess@infotec.mx>
Date:   Thu Oct 27 10:32:20 2022 -0500

    add: Se limpia tabla de repositorio remotos personales para su modificacion

[33mcommit 3e8f09b515aa16ab4de14e5a6c119b1e5fadf14a[m
Author: Jorge Enrique Cervantes Silva <jorge.cervantess@infotec.mx>
Date:   Thu Oct 27 10:04:08 2022 -0500

    add: Se agrega lista de repositorios remotos de el resto de los Devs

[33mcommit e88e315d8c590b5b8049eb9c05714853775b7493[m
Author: Jorge Enrique Cervantes Silva <jorge.cervantess@infotec.mx>
Date:   Thu Oct 6 15:40:50 2022 -0500

    doc: Se agrega documentacion de primera sesion practica de Curso de Git

[33mcommit 174ba48b8c34a5a64d902efb567160a576a37ab2[m
Merge: 234c59c 9a448db
Author: Jorge Cervantes <jorge.cervantes.silva@gmail.com>
Date:   Thu Feb 10 18:17:37 2022 +0000

    Merge branch 'develop' into 'master'
    
    Fix commits squash
    
    See merge request jorge.cervantes.silva/lineamientos!12

[33mcommit 9a448db0e5822b181b27f270d839bf2160062c1a[m
Merge: c7fd849 234c59c
Author: Jorge Enrique Cervantes Silva <jorge.cervantess@infotec.mx>
Date:   Thu Feb 10 12:15:36 2022 -0600

    Merge branch 'master' of https://gitlab.com/jorge.cervantes.silva/lineamientos into develop

[33mcommit 234c59c6f5853447e4f5ccb596c16d8d73deead2[m
Merge: eebc9c2 4518dd7
Author: Jorge Enrique Cervantes Silva <jorge.cervantess@infotec.mx>
Date:   Thu Feb 10 12:13:52 2022 -0600

    doc: Aumento de version 0.1.0

[33mcommit c7fd849862346859370fda68573a87846ee7a181[m
Merge: dc8471b eebc9c2
Author: Jorge Enrique Cervantes Silva <jorge.cervantess@infotec.mx>
Date:   Thu Feb 10 12:11:37 2022 -0600

    Merge branch 'master' of https://gitlab.com/jorge.cervantes.silva/lineamientos into develop

[33mcommit 4518dd7872e48a83669c4b33cca649adf76d5200[m
Merge: dc8471b 6e0855f
Author: Jorge Cervantes <jorge.cervantes.silva@gmail.com>
Date:   Thu Feb 10 18:07:29 2022 +0000

    Merge branch 'HU-LINEA-05' into 'develop'
    
    HU-LINEA-05
    
    See merge request jorge.cervantes.silva/lineamientos!6

[33mcommit 6e0855fe8088178c91d76e3a1f58e5743b11f640[m
Author: Jorge Enrique Cervantes Silva <jorge.cervantess@infotec.mx>
Date:   Thu Feb 10 12:04:09 2022 -0600

    doc: Aumento a la versión 0.1.0

[33mcommit 8623b489fa6f234b108c81e037ac5a50762e2cf7[m
Author: Vladimir Tagle <vladimir.tagle@infotec.mx>
Date:   Tue Feb 8 12:54:26 2022 -0600

    doc: Aumento a la versión 0.3.0

[33mcommit d93eda722fa1e80fafa98f193c3ee5c4a664d0bd[m
Author: Vladimir Tagle <vladimir.tagle@infotec.mx>
Date:   Tue Feb 8 12:39:25 2022 -0600

    add: Historia de Usuario 5

[33mcommit eebc9c29ffce2e1b76f6f29e74f1540054b9fc41[m
Merge: bcdfbfd f6eafce
Author: Jorge Cervantes <jorge.cervantes.silva@gmail.com>
Date:   Tue Feb 8 18:02:21 2022 +0000

    Merge branch 'develop' into 'master'
    
    Versión 0.0.1
    
    See merge request jorge.cervantes.silva/lineamientos!2

[33mcommit f6eafcebfe6bb0ed43e10cf232190617b689227b[m
Author: Jorge Cervantes <jorge.cervantes.silva@gmail.com>
Date:   Tue Feb 8 18:02:21 2022 +0000

    Versión 0.0.1

[33mcommit dc8471be091c26f572da13667b4880efa5962882[m
Merge: bcdfbfd f1c7e24
Author: Jorge Cervantes <jorge.cervantes.silva@gmail.com>
Date:   Tue Feb 8 17:42:41 2022 +0000

    Merge branch 'HU-LINEA-07' into 'develop'
    
    HU-LINEA-07
    
    See merge request jorge.cervantes.silva/lineamientos!1

[33mcommit f1c7e244d268aeea189e389743b43e9a20b14e0f[m
Author: jorge.cervantess <jorge.cervantess@infotec.mx>
Date:   Fri Feb 4 12:42:13 2022 -0600

    doc: Aumento de version 0.0.1

[33mcommit 84b06d6abb107c2b8f2b3e7ac3ad3d8e303451ee[m
Author: jorge.cervantess <jorge.cervantess@infotec.mx>
Date:   Fri Feb 4 12:38:34 2022 -0600

    add: Se agrega directorio para Merge Template de repositorio

[33mcommit b4b4a02b3f3a74e5809cf6ae2afef796d8376b01[m
Author: jorge.cervantess <jorge.cervantess@infotec.mx>
Date:   Fri Feb 4 12:37:58 2022 -0600

    add: Se agrega documento de registro de cambios.100

[33mcommit bcdfbfd57c8f3cd6cd65998464bb71a562d49948[m
Author: GitLab <root@localhost>
Date:   Wed Mar 6 09:52:24 2019 +0100

    Initial template creation
